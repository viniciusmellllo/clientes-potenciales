﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ClientesPotenciales.Filtro
{
    public class FiltroPorIndustria
    {
        private List<string> palabrasClave = new List<string>()
        {
            "banking", "financial", "service", "services", "capital markets",
            "market", "computer", "games", "consumer goods", "consumer goods design",
            "consumer", "design", "electrical", "electronic", "electronics", "manufacturing",
            "energy", "resources", "utilities", "entertainment", "sports", "government",
            "health", "health care", "techhuman", "information", "technology", "insurance",
            "media", "pharmaceuticals", "chemicals", "biotech", "publishing", "retail",
            "consumer", "products", "staffing", "recruiting", "telecom", "textiles", "nonprofit",
            "tolling", "automation", "travel", "transportation", "hospitality", "telecommunications",
            "environment", "renewable", "renewables", "internet", "computer", "software",
            "marketing", "advertising", "education", "supply chain", "supply"
        };

        public int CoincidePalabraClave(string industria)
        {
            var palabrasIndustria = industria.Trim().Split(" ");
            var vecesCoincideIndustria = 0;

            foreach (var palabra in palabrasIndustria)
            {
                if (palabrasClave.Any(s => palabra.Equals(s, StringComparison.CurrentCultureIgnoreCase)))
                {
                    vecesCoincideIndustria++;
                    continue;
                }
            }

            return vecesCoincideIndustria;
        }
    }
}
