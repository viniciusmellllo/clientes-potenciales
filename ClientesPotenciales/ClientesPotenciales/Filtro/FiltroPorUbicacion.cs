﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ClientesPotenciales.Filtro
{
    public class FiltroPorUbicacion
    {
        private List<string> ubicacionesBairesDev = new List<string>()
        {
            "United States", "Canada", "Argentina", "Mexico", "Brazil", "Cololmbia", "Spain"
        };

        public bool CoincideUbicacion(string industria)
        {
            if (ubicacionesBairesDev.Any(s => industria.Contains(s, StringComparison.CurrentCultureIgnoreCase)))
                return true;
            else
                return false;
        }
    }
}
