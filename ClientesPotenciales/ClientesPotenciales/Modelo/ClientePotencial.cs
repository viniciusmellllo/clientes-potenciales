﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientesPotenciales.Modelo
{
    public class ClientePotencial
    {
        #region atributos del cliente

        public long idPersona { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string rolActual { get; set; }
        public string pais { get; set; }
        public string industria { get; set; }
        public int numeroRecomendaciones { get; set; }
        public int numeroConexiones { get; set; }

        #endregion

        #region atributos para ordenar

        public bool tieneConexiones { get; set; }
        public bool tieneRecomendaciones { get; set; }
        public int vecesCoincideIndustria { get; set; }
        public bool coincideUbicacion { get; set; }

        #endregion
    }
}
