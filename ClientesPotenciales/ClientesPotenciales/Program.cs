﻿using ClientesPotenciales.Filtro;
using ClientesPotenciales.Modelo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ClientesPotenciales
{
    class Program
    {
        private static FiltroPorUbicacion filtroPorUbicacion = new FiltroPorUbicacion();
        private static FiltroPorIndustria filtroPorIndustria = new FiltroPorIndustria();

        static void Main(string[] args)
        {            
            var clientesPotenciales = new List<ClientePotencial>();

            try
            {
                Console.WriteLine("Empezando a leer el archivo...");

                var lineas = File.ReadAllLines(@"..\..\..\Recursos\people.in");

                foreach (string linea in lineas)
                {
                    var atributos = linea.Split('|');

                    if (atributos.Length > 0)
                    {
                        var tieneId = Int64.TryParse(atributos[0], out long number);
                        if (tieneId)
                        {
                            var cliente = ProcesaAtributosDeLaLinea(atributos);
                            clientesPotenciales.Add(cliente);
                        }
                    }
                }

                Console.WriteLine("Lectura de archivo terminada.");

                Console.WriteLine("Empezando ordenación de clientes potenciales según criterios definidos...");

                var clientesPotencialesOrdenados = clientesPotenciales.OrderByDescending(p0 => p0.tieneConexiones)
                    .ThenByDescending(p1 => p1.vecesCoincideIndustria)
                    .ThenByDescending(p2 => p2.coincideUbicacion)
                    .ThenByDescending(p3 => p3.tieneRecomendaciones)
                    .ThenByDescending(p4 => p4.numeroConexiones)
                    .ThenByDescending(p5 => p5.numeroRecomendaciones);

                Console.WriteLine("Ordenación finalizada.");

                Console.WriteLine("Empezando a escribir el archivo de salida...");

                using (StreamWriter archivoSalida = new StreamWriter(@"..\..\..\Recursos\people.out"))
                {
                    foreach (var cliente in clientesPotencialesOrdenados.Take(100))
                        archivoSalida.WriteLine(cliente.idPersona);
                }

                Console.WriteLine("Escrita de archivo terminada.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            Console.WriteLine("Presione cualquier tecla para salir.");
            Console.ReadKey();
        }

        private static ClientePotencial ProcesaAtributosDeLaLinea(string[] atributos)
        {
            var cliente = new ClientePotencial();
            cliente.idPersona = Int64.Parse(atributos[0]);

            if (atributos.Length > 1)
                cliente.nombre = string.IsNullOrEmpty(atributos[1]) ? "" : atributos[1];
            if (atributos.Length > 2)
                cliente.apellido = string.IsNullOrEmpty(atributos[2]) ? "" : atributos[2];
            if (atributos.Length > 3)
                cliente.rolActual = string.IsNullOrEmpty(atributos[3]) ? "" : atributos[3];

            if (atributos.Length > 4)
            {
                cliente.pais = string.IsNullOrEmpty(atributos[4]) ? "" : atributos[4];
                if (!string.IsNullOrEmpty(cliente.pais))
                    cliente.coincideUbicacion = filtroPorUbicacion.CoincideUbicacion(cliente.pais);
                else
                    cliente.coincideUbicacion = false;
            }

            if (atributos.Length > 5)
            {
                cliente.industria = string.IsNullOrEmpty(atributos[5]) ? "" : atributos[5];
                if (!string.IsNullOrEmpty(cliente.industria))
                    cliente.vecesCoincideIndustria = filtroPorIndustria.CoincidePalabraClave(cliente.industria);
            }

            if (atributos.Length > 6)
            {
                var tieneRecomendaciones = Int32.TryParse(atributos[6], out int number);
                if (tieneRecomendaciones)
                {
                    cliente.numeroRecomendaciones = Int32.Parse(atributos[6]);
                    if (cliente.numeroRecomendaciones > 0)
                        cliente.tieneRecomendaciones = true;
                }
            }

            if (atributos.Length > 7)
            {
                var tieneConexiones = Int32.TryParse(atributos[7], out int number);
                if (tieneConexiones)
                {
                    cliente.numeroConexiones = Int32.Parse(atributos[7]);
                    if (cliente.numeroConexiones > 0)
                        cliente.tieneConexiones = true;
                }
            }

            return cliente;
        }
    }
}
