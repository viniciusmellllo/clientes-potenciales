# README #

Este archivo contendrá todos los detalles del razonamiento utilizado.

### Tiempo de resolución ###

El tiempo dedicado a pensar, ensamblar los filtros y desarrollar fue de 5 horas.

### Razonamiento ###

1 - Hice una búsqueda en el sitio web de BairesDev para averiguar en qué sectores industriales opera BairesDev y en qué sectores industriales operan los clientes actuales.  
2 - Hice una búsqueda en el sitio web de BairesDev para averiguar en qué países BairesDev tiene una sede física.  
3 - Transformé los datos de la industria en un filtro de palabras clave, para contar el número de ocurrencias en el sector de cada cliente potencial.  
4 - Transformé los datos de los países anfitriones en un filtro de ubicación.  
5 - Prioricé a los clientes con conexiones y / o recomendaciones para evitar perfiles falsos o que no están en uso.  
6 - La ordenacíon fue: con conexiones -> con industria -> en los países con sede física -> con recomendaciones -> mas conexiones -> mas recomendaciones

### Cómo se podría mejorar? ###

1 - El filtro de la industria es de gran importancia debido a las historias de éxito. Podría poner peso en las palabras clave para seleccionar las más importantes.  
2 - No creo que funcione bien para archivos grandes. Podría pensar en una forma más sólida de desarrollo.

### Qué otra información sobre las personas te sería útil? ###

1 - Tiempo trabajando en este sector / empresa (para saber su nivel de influencia).  
2 - Número de empleados de la empresa (para estimar la capacidad de inversión y la generación de valor).